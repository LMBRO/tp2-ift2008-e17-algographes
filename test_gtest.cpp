/*
 * test_gtest.cpp
 *
 *  Created on: 2017-06-12
 *      Author: etudiant
 */

#include "gtest/gtest.h"
#include "Graphe.h"
#include "ReseauInterurbain.h"
#include "AssertionException.h"

class GrapheFixture1: public ::testing::Test {
public:

	GrapheFixture1( ):g1(4) {
       // initialization code here
		g1.donnerNomsParDefautApresIncluantSommet(0);

   }

   void SetUp( ) {
       // code here will execute just before the test ensues
   }

   void TearDown( ) {
       // code here will be called just after the test completes
       // ok to through exceptions from here if need be
   }

   ~GrapheFixture1( )  {
       // cleanup any pending stuff, but no exceptions allowed
   }
   TP2::Graphe g1;

   // put in any custom data members that you need
};
class GrapheFixture2: public ::testing::Test {
public:

	GrapheFixture2( ):g2(4) {
       // initialization code here
		g2.donnerNomsParDefautApresIncluantSommet(0);

   }
	void SetUp(){
		g2.ajouterArc(0,1,1.6,121.1);
		g2.ajouterArc(1,0,2.23,122.11);
		g2.ajouterArc(3,0,1.0,45.34);
		g2.ajouterArc(0,0,1,0);
   }
   TP2::Graphe g2;

   // put in any custom data members that you need
};
class GrapheFixture3Vide: public ::testing::Test {
public:

	GrapheFixture3Vide( ):g3(0) {
       // initialization code here
		//g3.donnerNomsParDefautApresIncluantSommet(0); //Lance une exception?

   }
   TP2::Graphe g3;

   // put in any custom data members that you need
};
class GrapheFixture4: public ::testing::Test {
public:

	GrapheFixture4( ):g4(4) {
       // initialization code here
		g4.ajouterArc(0,1,1.6,121.1);
		g4.ajouterArc(1,0,2.23,122.11);
		g4.ajouterArc(3,0,1.0,45.34);
		g4.ajouterArc(0,0,1,0);
		g4.nommer(0,"Nom1");
		g4.nommer(1,"Nom2");
		g4.nommer(2,"Nom3");
		g4.nommer(3,"Nom4");

   }
   TP2::Graphe g4;

   // put in any custom data members that you need
};
TEST_F(GrapheFixture1, resize1){

	ASSERT_EQ(g1.getNombreSommets(),4);
	g1.resize(2);
	ASSERT_EQ(g1.getNombreSommets(),2);
	g1.resize(3000);
	ASSERT_EQ(g1.getNombreSommets(),3000);
	g1.resize(0);
	ASSERT_EQ(g1.getNombreSommets(),0);
	ASSERT_THROW(g1.resize(-1),std::length_error);
}
TEST_F(GrapheFixture1, nomDefaut){
	for(int i = 0;i != g1.getNombreSommets();++i){
		ASSERT_EQ(std::to_string(i+1), g1.getNomSommet(i));
	}
}
TEST_F(GrapheFixture1, ajouterArc){
	std::cout << "@@@@@@@@@@@@@@@@@@" << std::endl;
	std::cout << g1 << std::endl;
	std::cout << "@@@@@@@@@@@@@@@@@@" << std::endl;
	g1.ajouterArc(0,1,1.6,121.1);
	g1.ajouterArc(1,0,2.23,122.11);
	g1.ajouterArc(3,0,1.0,45.34);
	g1.ajouterArc(0,0,1,0);
	std::cout << "@@@@@@@@@@@@@@@@@@" << std::endl;
	std::cout << g1 << std::endl;
	std::cout << "@@@@@@@@@@@@@@@@@@" << std::endl;
	ASSERT_EQ(g1.arcExiste(0,1),true);
	ASSERT_EQ(g1.arcExiste(1,0),true);
	ASSERT_EQ(g1.arcExiste(3,0),true);
	ASSERT_EQ(g1.arcExiste(0,3),false);
	ASSERT_EQ(g1.arcExiste(0,0),true);
	ASSERT_EQ(g1.arcExiste(3,3),false);
}
TEST_F(GrapheFixture2,enleverArc){
	ASSERT_EQ(g2.arcExiste(0,0),true);
	ASSERT_EQ(g2.arcExiste(0,1),true);
	ASSERT_EQ(g2.arcExiste(1,0),true);
	g2.enleverArc(0,0);
	g2.enleverArc(0,1);
	ASSERT_EQ(g2.arcExiste(0,0),false);
	ASSERT_EQ(g2.arcExiste(0,1),false);

}
TEST_F(GrapheFixture2,listerSommetsAdjacents){
	std::vector<size_t> v{1,0};
	std::vector<size_t> v2;
	ASSERT_EQ(g2.listerSommetsAdjacents(0), v);
	ASSERT_EQ(g2.listerSommetsAdjacents(2), v2);
}
TEST_F(GrapheFixture2, getNomSommet){
	ASSERT_EQ(g2.getNomSommet(0),"1");
	ASSERT_EQ(g2.getNomSommet(1),"2");
	ASSERT_EQ(g2.getNomSommet(2),"3");
	ASSERT_EQ(g2.getNomSommet(3),"4");

	ASSERT_THROW(g2.getNomSommet(-1), AssertionException);
	ASSERT_THROW(g2.getNomSommet(4), AssertionException);
}
TEST_F(GrapheFixture2, getNumeroSommet){
	ASSERT_EQ(g2.getNumeroSommet("1"),0);
	ASSERT_EQ(g2.getNumeroSommet("2"),1);
	ASSERT_EQ(g2.getNumeroSommet("3"),2);
	ASSERT_EQ(g2.getNumeroSommet("4"),3);

	ASSERT_THROW(g2.getNumeroSommet("Test"),AssertionException);

}
TEST_F(GrapheFixture3Vide, getNumeroSommetVide){
	ASSERT_THROW(g3.getNumeroSommet("1"),AssertionException);
}
TEST_F(GrapheFixture2,getNombreSommets){
	ASSERT_EQ(g2.getNombreSommets(),4);
}
TEST_F(GrapheFixture3Vide,getNombreSommetsVide){
	ASSERT_EQ(g3.getNombreSommets(),0);
}
TEST_F(GrapheFixture2,getNombreArcs){
	ASSERT_EQ(g2.getNombreArcs(),4);
}
TEST_F(GrapheFixture3Vide,getNombreArcsVide){
	ASSERT_EQ(g3.getNombreArcs(),0);
}
TEST_F(GrapheFixture2, getPonderationsArc){
	ASSERT_FLOAT_EQ(g2.getPonderationsArc(0,0).duree,1.0);
	ASSERT_FLOAT_EQ(g2.getPonderationsArc(0,0).cout,0.0);

	ASSERT_FLOAT_EQ(g2.getPonderationsArc(0,1).duree,1.6);
	ASSERT_FLOAT_EQ(g2.getPonderationsArc(0,1).cout,121.1);

	ASSERT_FLOAT_EQ(g2.getPonderationsArc(1,0).duree,2.23);
	ASSERT_FLOAT_EQ(g2.getPonderationsArc(1,0).cout,122.11);

	ASSERT_THROW(g2.getPonderationsArc(0,3), AssertionException);
}

TEST_F(GrapheFixture3Vide,getPonderationsArcVide){
	ASSERT_THROW(g3.getPonderationsArc(0,3), AssertionException);
}
TEST_F(GrapheFixture4, nomsParDefautPartiels){
	g4.donnerNomsParDefautApresIncluantSommet(2);
	ASSERT_EQ(g4.getNomSommet(0),"Nom1");
	ASSERT_EQ(g4.getNomSommet(1),"Nom2");
	ASSERT_EQ(g4.getNomSommet(2),"3");
	ASSERT_EQ(g4.getNomSommet(3),"4");

	g4.donnerNomsParDefautApresIncluantSommet(0);
	ASSERT_EQ(g4.getNomSommet(0),"1");
	ASSERT_EQ(g4.getNomSommet(1),"2");
	ASSERT_EQ(g4.getNomSommet(2),"3");
	ASSERT_EQ(g4.getNomSommet(3),"4");

	ASSERT_THROW(g4.donnerNomsParDefautApresIncluantSommet(-1),AssertionException);
	ASSERT_THROW(g4.donnerNomsParDefautApresIncluantSommet(5),AssertionException);

	g4.resize(6);
	ASSERT_EQ(g4.getNomSommet(4),"5");
	ASSERT_EQ(g4.getNomSommet(5),"6");
}
TEST_F(GrapheFixture3Vide, nomsParDefautVide){
	ASSERT_THROW(g3.donnerNomsParDefautApresIncluantSommet(0),AssertionException);
	ASSERT_THROW(g3.donnerNomsParDefautApresIncluantSommet(-1),AssertionException);
	ASSERT_THROW(g3.donnerNomsParDefautApresIncluantSommet(1),AssertionException);
}

