/**
 * \file Principal.cpp
 * \brief Fichier pour les différents tests
 * \author IFT-2008
 * \version 0.5
 * \date juin 2017
 *
 */

#include <iostream>
#include <fstream>
#include <ctime>

#include "ReseauInterurbain.h"

using namespace std;
using namespace TP2;

int main()
{

	ReseauInterurbain reseau("");	//Le reseau utilisé pour les tests.
	ifstream EntreeFichier;			//Flux d'entrée
	EntreeFichier.open("graphe.txt", ios::in);
		if(EntreeFichier.is_open())
		{
			reseau.chargerReseau(EntreeFichier);
		}
		EntreeFichier.close();
		cout << "Le reseau a ete charge !" << endl;
		cout << "Affichage du reseau: " << endl << reseau;

		cout << "Recherche composantes fortement connexes avec Kosaraju." << endl;
		std::vector<std::vector<std::string> > resultat = reseau.algorithmeKosaraju();
		cout << "nombre des composantes trouvees: " << resultat.size() << endl;
		for (size_t i = 0; i < resultat.size(); ++i) {
			cout << "Composante numero " << i+1 << ": " << endl;
			for (auto itr = resultat.at(i).begin(); itr != resultat.at(i).end(); ++itr)
				cout << *itr << ", ";
			cout << endl;
		}
	return 0;
}
