/*
 * altPrincipal.cpp
 *
 *  Created on: 2017-06-14
 *      Author: etudiant
 */

#include <iostream>
#include <fstream>
#include <ctime>

#include "ReseauInterurbain.h"

using namespace std;
using namespace TP2;

int main()
{

	int nb = 1;						//Choix de l'utilisateur dans le menu initialisé à 1.
	string villeDepart;				//Chaîne de caractères représentant la ville de départ.
	string villeDestination;		//Chaîne de caractères représentant la ville de d'arrivée.
	ReseauInterurbain reseau("");	//Le reseau utilisé pour les tests.
	ifstream EntreeFichier;			//Flux d'entrée
	ofstream SortieFichier;			//Flux d'entrée
	clock_t startTime, endTime;  	//Pour mesurer l'efficacité des algorithmes

	EntreeFichier.open("monreseau.txt", ios::in);
	if(EntreeFichier.is_open())
	{
		reseau.chargerReseau(EntreeFichier);
	}
	EntreeFichier.close();
	cout << "Le reseau a ete charge !" << endl;
	cout << "Affichage du reseau: " << endl << reseau;
	Chemin ch1_dijkstra = reseau.rechercheCheminDijkstra("v1","v7", true);
	if(ch1_dijkstra.reussi)
	{
		cout << "Liste des villes du plus court chemin en utilisant la duree du trajet:" << endl;
		for (auto itr = ch1_dijkstra.listeVilles.begin(); itr!= ch1_dijkstra.listeVilles.end(); ++itr)
			cout << *itr << ", ";
		cout << endl << "Duree totale du plus court chemin: " << ch1_dijkstra.dureeTotale << endl;
	}
	else
		cout << "Pas de chemin trouve !" << endl;
	return 0;
}


