/**
 * \file ReseauInterurbain.cpp
 * \brief Implémentattion de la classe ReseauInterurbain.
 * \author ?
 * \version 0.5
 * \date ?
 *
 *  Travail pratique numéro 2
 *
 */
#include <sstream>
#include <fstream>
#include "ReseauInterurbain.h"
#include "AssertionException.h"

//vous pouvez inclure d'autres librairies si c'est nécessaire

namespace TP2
{
ReseauInterurbain::ReseauInterurbain(std::string nomReseau, size_t nbVilles):nomReseau{nomReseau},unReseau{nbVilles}{};
// Méthode fournie
void ReseauInterurbain::chargerReseau(std::ifstream & fichierEntree)
{
	std::string buff;

	getline(fichierEntree, nomReseau);
	nomReseau.erase(0, 20); // Enlève: Reseau Interurbain:

	int nbVilles;

	fichierEntree >> nbVilles;
	getline(fichierEntree, buff); //villes

	unReseau.resize(nbVilles);

	getline(fichierEntree, buff); //Liste des villes

	size_t i = 0;

	while(bool(getline(fichierEntree, buff)) && buff != "Liste des trajets:")
	{
		unReseau.nommer(i, buff);
		i++;
	}

	while(getline(fichierEntree, buff))
	{
		std::string source = buff;
		getline(fichierEntree, buff);
		std::string destination = buff;

		getline(fichierEntree, buff);
		std::istringstream iss(buff);

		float duree;
		iss >> duree;

		float cout;
		iss >> cout;

		unReseau.ajouterArc(unReseau.getNumeroSommet(source), unReseau.getNumeroSommet(destination), duree, cout);
	}
}
void ReseauInterurbain::resize(size_t nouvelleTaille){
	unReseau.resize(nouvelleTaille);
}
Chemin ReseauInterurbain::rechercheCheminDijkstra(const std::string& source, const std::string& destination, bool dureeCout) const{
	//Pas le plus efficace. Aussi, ce serait mieux d'enregistrer toutes les plus courtes distances
	//met on ne peut pas ajouter de nouveau attributs aux classes.
	ASSERTION(unReseau.getNombreSommets() != 0);
	ASSERTION(source != destination);
	ASSERTION(unReseau.sommetExiste(source));
	ASSERTION(unReseau.sommetExiste(destination));

	Chemin chemin{};
	std::vector<std::string> vecVilles;
	size_t nbSommets = unReseau.getNombreSommets();
	size_t nsource = unReseau.getNumeroSommet(source);
	size_t ndest = unReseau.getNumeroSommet(destination);
	std::vector<bool> distanceConnue(nbSommets, false);
	std::vector<int> predecesseurs(nbSommets, -1);
	std::vector<float> distances(nbSommets,std::numeric_limits<float>::max() );
	distances[nsource] = 0.0;
	auto cmp = [](ElemQueue egauche,ElemQueue edroite){
		return egauche.distance > edroite.distance;
	};
	std::priority_queue<ElemQueue, std::vector<ElemQueue>,decltype(cmp)> q{cmp};
	for(size_t s = 0; s < nbSommets;++s){
		q.push(ElemQueue{s,(s == nsource)? 0.0f : std::numeric_limits<float>::max()});
	}
	bool destinationTrouvee = false;
	bool limiteInaccessible = false;
	for(size_t x = 0;x < nbSommets;++x){ //boucle principale
		ElemQueue e  = q.top();
		q.pop();
		while(distanceConnue[e.sommet] ){
			//Sommet dont la distance connue: c'est un doublon dans la file de priorité, on la passe

			ElemQueue temp = q.top();
			q.pop();
			e.distance = temp.distance;
			e.sommet = temp.sommet;
		}
		if(limiteInaccessible and predecesseurs[e.sommet] == -1){
			chemin.reussi = false;
			return chemin;
		}
		limiteInaccessible = true;
		// apres avoir dépiler la source et mis à jour les adjacents de la source, si on trouve un sommet
		// qu'on n'a pas encore rencontré (on a pas assigné de prédecesseur, et donc que la dest n'a pas
		// encore été vue)
		//on sait qu'on ne rencontrera pas la destination parce qu'elle est dans
		//une composante fortement connexe innaccessible depuis la source.
		//Comme on ne s'intéresse qu'au chemin entre source et destination, on peut retourner tout de suite.

		size_t s = e.sommet;
		if(s == ndest){
			destinationTrouvee = true;
			break;
		}
		distanceConnue[s] = true;
		std::vector<size_t> adjs = unReseau.listerSommetsAdjacents(s);
		for( size_t u: adjs){
			if(!distanceConnue[u]){ //arc <s,s> est donc ignoré automatiquement
				Ponderations pond = unReseau.getPonderationsArc(s,u);
				float dist_arc = (dureeCout) ? pond.duree : pond.cout;
				if(e.distance + dist_arc < distances[u] ){
					q.push(ElemQueue{u,e.distance + dist_arc});
					distances[u] = e.distance + dist_arc;
					predecesseurs[u] = s;
				}
			}
		}
	}// fin boucle principale
	if(destinationTrouvee){
		chemin.reussi = true;
		if(dureeCout){
			chemin.dureeTotale = distances[ndest];
			chemin.coutTotal = -1.0;
		}
		else{
			chemin.dureeTotale = -1.0;
			chemin.coutTotal = distances[ndest];
		}
		std::stack<std::string> pile{};

		std::string s = destination;
		size_t npred = ndest;

		while(s != source){
			pile.push(s);
			npred = predecesseurs[npred];
			s = unReseau.getNomSommet(npred);
		}
		chemin.listeVilles.push_back(s); //nom du sommet de départ;
		while(!pile.empty()){
			chemin.listeVilles.push_back(pile.top());
			pile.pop();
		}
		return chemin;
	}
	//cas où il y a pas de chemin est géré dans la boucle principale.
}
void ReseauInterurbain::explore(size_t v,std::vector<bool>& estVisite, std::vector<size_t>& composante) const{
	estVisite[v] = true;
	composante.push_back(v);
	std::vector<size_t> adjs = unReseau.listerSommetsAdjacents(v);
	for(size_t u:adjs){
		if(!estVisite[u])
			explore(u,estVisite,composante);
	}
}
std::vector<std::vector<std::string> > ReseauInterurbain::algorithmeKosaraju(){
	using std::stack;
	using std::vector;
	using std::string;
	std::vector<std::vector<std::string> > composantes{};
	Graphe gi = unReseau.getGrapheInverse();
	stack<size_t> pileSomAband = gi.parcoursProfondeur(0); //Le noeud choisi ici n'a pas d'importance
	vector<bool> estVisite(unReseau.getNombreSommets(),false);
	while(!pileSomAband.empty()){
		size_t v = pileSomAband.top();
		pileSomAband.pop();
		if (!estVisite[v]){
			vector<size_t> comp{};
			explore(v,estVisite,comp);
			std::vector<string> nomsComposante(comp.size());
			std::transform(comp.begin(),comp.end(),nomsComposante.begin(),[this](size_t u)->string{return this->unReseau.getNomSommet(u);});
			composantes.push_back(nomsComposante);
		}
	}
	return composantes;



}

//À compléter par l'implémentation des autres méthodes demandées

}//Fin du namespace
