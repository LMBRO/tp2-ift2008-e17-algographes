/**
 * \file Graphe.cpp
 * \brief Implémentation d'un graphe orienté.
 * \author ?
 * \version 0.5
 * \date ?
 *
 *  Travail pratique numéro 2
 *
 */

#include "Graphe.h"
#include "AssertionException.h"
#include <algorithm>
//vous pouvez inclure d'autres librairies si c'est nécessaire

namespace TP2
{
	//Mettez l'implémentation de vos méthodes ici.
	Graphe::Graphe(size_t nbSommets):m_noms(nbSommets),m_listesAdj(nbSommets),m_nbSommets{nbSommets},m_nbArcs{0} {
	}

	void Graphe::resize(size_t nouvelleTaille) {
		//Si l'argument passé est négatif il y a conversion automatique et risque que std::length_error soit lancée si la valeur résultante
		//est trop grande.
		size_t ancienNbSommets = getNombreSommets();
		if(nouvelleTaille == ancienNbSommets)
			return;
		m_listesAdj.resize(nouvelleTaille);
		m_noms.resize(nouvelleTaille);
		m_nbSommets = nouvelleTaille;

		if(nouvelleTaille > ancienNbSommets){
			donnerNomsParDefautApresIncluantSommet(ancienNbSommets);
		}
		else{
			for(std::list<Arc>  & la : m_listesAdj){
				la.remove_if([this](Arc const & a){return a.destination >= m_nbSommets;});
			}
		}
		verifierInvariant();
	}

	void Graphe::nommer(size_t sommet, const std::string& nom) {
		ASSERTION(nom != "");
		ASSERTION(getNombreSommets() != 0 and sommet <= m_nbSommets - 1 );
		m_noms[sommet] = nom;
		//veifierInvariant();
	}
	void Graphe::donnerNomsParDefautApresIncluantSommet(size_t sommetDebut){
		ASSERTION(getNombreSommets() != 0 and sommetDebut <= m_nbSommets - 1 );
		for(size_t i = sommetDebut;i!= m_nbSommets;++i){
			m_noms[i] = std::to_string(i+1);
		}
	}
	void Graphe::ajouterArc(size_t source, size_t destination, float duree,
			float cout) {
		ASSERTION(!arcExiste(source, destination));
		ASSERTION(duree > 0.0);
		ASSERTION(cout >= 0.0);
		m_listesAdj[source].push_back(Arc{destination, Ponderations{duree,cout}});
		++m_nbArcs;
		verifierInvariant();
	}

	void Graphe::enleverArc(size_t source, size_t destination) {
		ASSERTION(arcExiste(source, destination));
		m_listesAdj[source].remove_if([destination](Arc const & a)-> bool{return a.destination == destination;});
		--m_nbArcs;
		verifierInvariant();
	}

	bool Graphe::arcExiste(size_t source, size_t destination) const {
		ASSERTION(getNombreSommets() != 0);
		ASSERTION(source <= m_nbSommets - 1);
		ASSERTION(destination <= m_nbSommets - 1);
		for(Arc const & arc : m_listesAdj[source]){
			if(arc.destination == destination)
				return true;
		}
		return false;

	}

	std::vector<size_t> Graphe::listerSommetsAdjacents(size_t sommet) const {
		ASSERTION(sommet <= m_nbSommets - 1);
		std::vector<size_t> ret_adj{};
		for(Arc const & a: m_listesAdj[sommet]){
			ret_adj.push_back(a.destination);
		}
		return ret_adj;
	}

	std::string Graphe::getNomSommet(size_t sommet) const {
		ASSERTION(sommet <= m_nbSommets - 1);
		return m_noms[sommet];
	}

	size_t Graphe::getNumeroSommet(const std::string& nom) const {
		std::vector<std::string>::const_iterator cit;
		#if !defined(NDEBUG)
			ASSERTION(m_noms.cend() != (cit = std::find(m_noms.cbegin(),m_noms.cend(),nom)));
		#else
			cit = std::find(m_noms.cbegin(),m_noms.cend(),nom)
		#endif
		return cit - m_noms.cbegin();
	}

	int Graphe::getNombreSommets() const {
		return m_nbSommets;
	}

	int Graphe::getNombreArcs() const {
		size_t compte_arc = 0;
		for(std::list<Arc> const & la : m_listesAdj){
			for(Arc const & a : la){
				++compte_arc;
			}
		}
		return compte_arc;
	}

	Ponderations Graphe::getPonderationsArc(size_t source, //possibilité d'améliorer?
			size_t destination) const {
		ASSERTION(arcExiste(source,destination));
		return std::find_if(m_listesAdj[source].cbegin(),m_listesAdj[source].cend(),
				[destination](Arc const & a)-> bool{return a.destination == destination;})->poids;
	}

	void Graphe::verifierInvariant() const {
		ASSERTION(m_nbSommets == m_noms.size() and m_nbSommets == m_listesAdj.size());
		size_t compte_arc = 0;
		for(std::list<Arc> const & la : m_listesAdj){
			for(Arc const & a : la){
				++compte_arc;
			}
		}
		ASSERTION(compte_arc == m_nbArcs);
	}

	bool Graphe::sommetExiste(const std::string& nom) const {
		return m_noms.cend() != std::find(m_noms.cbegin(),m_noms.cend(),nom);
	}
	void Graphe::explore(size_t v, std::stack<size_t> & pile,std::vector<bool> & estVisite) const{
		estVisite[v] = true;
		std::vector<size_t> adjs = listerSommetsAdjacents(v);
		for(size_t u:adjs){
			if(!estVisite[u])
				explore(u,pile,estVisite);
		}
		pile.push(v);
	}
	std::stack<size_t> Graphe::parcoursProfondeur(size_t v) const{
		ASSERTION(v <= getNombreSommets() - 1);
		using std::stack;
		using std::vector;
		stack<size_t> pile{};
		vector<bool> estVisite(getNombreSommets(),false);
		explore(v,pile,estVisite);
		for(size_t s = 0;s != getNombreSommets();++s){
			if(!estVisite[s])
				explore(s,pile,estVisite);
		}
		return pile;



		}
	Graphe Graphe::getGrapheInverse() const{
		Graphe inverse = *this;
		for(std::list<Arc> & la: inverse.m_listesAdj){
			la.erase(la.begin(),la.end());
		}
		inverse.m_nbArcs = 0;
		for(size_t i = 0;i != m_listesAdj.size();++i){
			std::list<Arc> const & la = m_listesAdj[i];
			for(Arc const & a : la){
				inverse.ajouterArc(a.destination, i, a.poids.duree, a.poids.cout);
			}
		}
		return inverse;
	}
}		//Fin du namespace

